# Group : 3.1
# Name 1 : DERIEUX Jean	

EXEC= dac

.PHONY: all clean mrproper

all: $(EXEC)
	

dac: dac.o
	gcc -g -Wall -Iinclude dac.o -o dac
dac.o: dac.c
	gcc -g -Wall -Iinclude -c dac.c -o dac.o



clean:
	rm -rf *.o

mrproper: clean
	rm -rf $(EXEC)







